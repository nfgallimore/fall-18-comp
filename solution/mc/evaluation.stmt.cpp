#include "evaluation.hpp"
#include "stmt.hpp"
#include "decl.hpp"

#include <iostream>

static Control
eval_block(Evaluation& eval, Block_stmt const* s)
{
  for (Stmt const* child : s->get_children()) {
    Control ctrl = evaluate_stmt(eval, child);

    // If the sub-statement breaks, continues, or returns, then return
    // that control the the enclosing statement.
    if (ctrl != next_fc)
      return ctrl;
  }

  return next_fc;
}

static Control
eval_if(Evaluation& eval, If_stmt const* s)
{
  Value cond = evaluate_expr(eval, s->get_condition());
  if (cond.get_int())
    return evaluate_stmt(eval, s->get_true_statement());
  else
    return evaluate_stmt(eval, s->get_false_statement());
}

static Control
eval_while(Evaluation& eval, While_stmt const* s)
{
  while (true) {
    Value cond = evaluate_expr(eval, s->get_condition());
    if (cond.get_int()) {
      Control ctrl = evaluate_stmt(eval, s->get_body());
      if (ctrl == return_fc)
        return ctrl;
      if (ctrl == break_fc)
        break;
      if (ctrl == cont_fc)
        continue;
    }
    else {
      break;
    }
  }
  return next_fc;
}

static Control
eval_break(Evaluation& eval, Break_stmt const* s)
{
  return break_fc;
}

static Control
eval_cont(Evaluation& eval, Cont_stmt const* s)
{
  return cont_fc;
}

static Control
eval_ret(Evaluation& eval, Ret_stmt const* s)
{
  // Evaluate the return value.
  Value val = evaluate_expr(eval, s->get_return_value());

  // Lookup the returned object.
  Frame* frame = eval.get_current_frame();
  Fn_decl* fn = static_cast<Fn_decl*>(frame->get_function());
  Object* ret = frame->locate_local(fn->get_return());

  // Copy initialize the return object.
  ret->initialize(val);

  return return_fc;
}

static Control
eval_expr(Evaluation& eval, Expr_stmt const* s)
{
  evaluate_expr(eval, s->get_expression());

  return next_fc;
}

static Control
eval_decl(Evaluation& eval, Decl_stmt const* s)
{
  // FIXME: Not a sane assumption.
  Var_decl* decl = static_cast<Var_decl*>(s->get_declaration());
  
  // Note that the object should be allocated in the frame. Assuming
  // this isn't local.
  Object* obj = eval.get_current_frame()->get_locals().locate(decl);
  
  // Evaluate the initializer.
  Value val = evaluate_expr(eval, decl->get_initializer());
  
  obj->initialize(val);

  return next_fc;
}

Control
evaluate_stmt(Evaluation& eval, Stmt const* s)
{
  switch (s->get_kind()) {
  case Stmt::skip_stmt:
    return next_fc;
  case Stmt::block_stmt:
    return eval_block(eval, static_cast<Block_stmt const*>(s));
  case Stmt::if_stmt:
    return eval_if(eval, static_cast<If_stmt const*>(s));
  case Stmt::while_stmt:
    return eval_while(eval, static_cast<While_stmt const*>(s));
  case Stmt::break_stmt:
    return eval_break(eval, static_cast<Break_stmt const*>(s));
  case Stmt::cont_stmt:
    return eval_cont(eval, static_cast<Cont_stmt const*>(s));
  case Stmt::ret_stmt:
    return eval_ret(eval, static_cast<Ret_stmt const*>(s));
  case Stmt::expr_stmt:
    return eval_expr(eval, static_cast<Expr_stmt const*>(s));
  case Stmt::decl_stmt:
    return eval_decl(eval, static_cast<Decl_stmt const*>(s));
  }
  assert(false);
}
