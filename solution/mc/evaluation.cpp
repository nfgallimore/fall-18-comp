#include "evaluation.hpp"
#include "decl.hpp"

Object*
Evaluation::allocate_static(Decl* d)
{
  return m_globals.allocate(d);
}

Object*
Evaluation::allocate_automatic(Decl* d)
{
  return get_current_frame()->allocate_local(d);
}

static Monotonic_store&
get_store(Evaluation& eval, int store)
{
  if (store == -1)
    return eval.get_globals();
  else
    return eval.get_stack().get_frame(store)->get_locals();
}

Object*
Evaluation::locate_object(Value const& val)
{
  Addr_value addr = val.get_address();
  Monotonic_store& store = get_store(*this, addr.store);
  return store.locate(addr.def);
}

Decl*
Evaluation::get_current_function() const
{
  return get_current_frame()->get_function();
}

Frame*
Evaluation::push_frame(Decl* d)
{
  assert(d->is_function());
  Fn_decl* fn = static_cast<Fn_decl*>(d);

  // Push the frame onto the stack.
  Frame* frame = m_stack.push(fn);

  // Activate the frame by allocating local objects for each 
  // parameter of the function, including reference parameters. 
  // Note that reference parameters are actually objects that 
  // hold address values (i.e., pointers).
  for (Decl* local : fn->get_children()) {
    if (!local->is_variable())
      continue;

    Var_decl* var = static_cast<Var_decl*>(local);
    frame->allocate_local(var);
  }
  
  return frame;
}

void
Evaluation::pop_frame()
{
  m_stack.pop();
}
