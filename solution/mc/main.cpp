#include <iostream>

#include "name.hpp"
#include "type.hpp"
#include "expr.hpp"
#include "stmt.hpp"
#include "decl.hpp"
#include "value.hpp"
#include "builder.hpp"
#include "evaluation.hpp"

Decl* 
make_min(Builder& build)
{
  Type* b = build.get_bool_type();
  Type* z = build.get_int_type();
 
  // Build parameters and return value.
  Decl* p1 = build.make_variable(build.get_name("a"), z);
  Decl* p2 = build.make_variable(build.get_name("b"), z);
  Decl* ret = build.make_variable(nullptr, z);

  // Build the expression p1 < p2 ? p1 : p2
  Expr* expr = build.make_conditional(
    build.make_lt(
      build.make_id(p1), 
      build.make_id(p2)),
    build.make_id(p1),
    build.make_id(p2)
  );

  Stmt* body = build.make_block({
    build.make_return(ret, expr)
  });

  // Build the function type.
  Type* ft = build.get_function_type({z, z, z});

  // Build the function.
  Fn_decl* fn = build.make_function(build.get_name("min"), ft);
  fn->set_parameters({p1, p2});
  fn->set_return(ret);
  fn->set_body(body);

  return fn;
}

Decl* 
make_fact(Builder& build)
{
  // fun fact(n : int) { 
  //   var x : int = 1;
  //   while (n != 0) {
  //     x = x * n;
  //     n = n - 1;
  //   }
  // }

  Type* z = build.get_int_type();
 
  // Build the function declaration.
  Decl* n = build.make_variable(build.get_name("n"), z);
  Decl* r = build.make_variable(nullptr, z);
  Type* ft = build.get_function_type({z, z}); // (int)->(int)
  Fn_decl* fn = build.make_function(build.get_name("fact"), ft);
  fn->set_parameters({n});
  fn->set_return(r);

  // Create locals. We have to do this outside the body because we 
  // have references throughout the body and there's no clean way to 
  // extract references on the fly.
  Decl* x = build.make_variable(build.get_name("x"), z);
  build.copy_initialize(x, build.make_int(1));

  // Build the body.
  Stmt* body = build.make_block({
    build.make_declaration(x),
    build.make_while(
      build.make_ne(build.make_id(n), build.make_int(0)),
      build.make_block({
        build.make_expression(
          build.make_assign(
            build.make_id(x), 
            build.make_mul(build.make_id(x), build.make_id(n)))),
        build.make_expression(
          build.make_assign(
            build.make_id(n), 
            build.make_sub(build.make_id(n), build.make_int(0))))
      }))
  });
  
  fn->set_body(body);

  return fn;
}


int
main()
{
  Builder build;

  // Min
  {
    // Declare some functions
    Decl* min = make_min(build);
    min->debug();

    // min(3, 4)
    Expr* e = build.make_call({
      build.make_id(min),
      build.make_int(3),
      build.make_int(4),
    });
    e->debug();

    Evaluation eval;
    Value val = evaluate_expr(eval, e);
    std::cout << "min(3, 4) == " << val << '\n';
  }
  
  // Factorial
  {
    Decl* fact = make_fact(build);
    fact->debug();

    // min(3, 4)
    Expr* e = build.make_call({
      build.make_id(fact),
      build.make_int(10)
    });

    Evaluation eval;
    Value val = evaluate_expr(eval, e);
  }
  
}
